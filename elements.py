import main


driver = main.getDriver()

def getCareerBtn():
    main.waite(1)
    return driver.find_element_by_link_text("Карьера")

def getFillFormBtn():
    main.waite(1)
    return driver.find_element_by_link_text("Заполнить анкету")

def getDownloadResumeBtn():
    main.waite(1)
    return driver.find_element_by_id("upload")
    # return driver.find_element_by_name("up_file")

def getDownloadResumeInput():
    main.waite(1)
    return driver.find_element_by_name("up_file")

def getResumeErrorLbl():
    main.waite(1)
    return driver.find_element_by_id("up_file_name").find_element_by_class_name("control-label")

def getMyNameInput():
    main.waite(1)
    return driver.find_element_by_id("inputName")

def getMyLastNameInput():
    main.waite(1)
    return driver.find_element_by_id("inputLastname")

def getMyEmailInput():
    main.waite(1)
    return driver.find_element_by_id("inputEmail")

def getMyPhoneNumInput():
    main.waite(1)
    return driver.find_element_by_id("inputPhone")

def getSendBtn():
    main.waite(1)
    return driver.find_element_by_id("submit")

def getNetpeakLogo():
    main.waite(1)
    return driver.find_element_by_xpath("//img[contains(@src,'netpeak_white_ru.svg')]")

def getMainTitle():
    main.waite(1)
    return driver.find_element_by_xpath("//*[contains(@class, 'screen-title')]")

def getWarningFieldLbl():
    main.waite(1)
    return driver.find_element_by_xpath("//*[contains(@class, 'warning-fields')]")

def getBirthDayPopup():
    main.waite(1)
    return driver.find_element_by_xpath("//*[contains(@data-error-name, 'Birth day')]")

def getBirthMonthPopup():
    main.waite(1)
    return driver.find_element_by_xpath("//*[contains(@data-error-name, 'Birth month')]")

def getBirthYearPopup():
    main.waite(1)
    return driver.find_element_by_xpath("//*[contains(@data-error-name, 'Birth year')]")

def selectNumFromPopup(elem, num):
    elem.find_element_by_xpath("//option[@value='" + num + "']").click