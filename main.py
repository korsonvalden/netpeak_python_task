from selenium import webdriver
import string
import random
import time



driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)

def waite(sec=None):
    if sec==None: sec=10
    time.sleep(sec)

def getDriver():
    return driver

def closeDriver():
    driver.close()

def waitForText(elem, sec, t):
    text = elem.text
    time_temp = 0
    while time_temp <= sec:
        for i in range(sec, 0, -1):
            text = elem.text
            if t in text:
                return text
            time.sleep(1)
    Exception("Searched text: " + t + " - not found. Current text: " + text)

def randomString(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def randomEmail(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength)) + "@" + ''.join(random.choice(letters) for i in range(stringLength)) + ".com.ua"

def randomInt():
    return random.randint(100000000, 999999999)

def randomDay():
    return random.randint(1, 31)

def randomMonth():
    months = ["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"]
    return random.choice(months)

def randomYear():
    return random.randint(1960, 2010)

def checkEquality(t1, t2):
    if t1 == t2: return True
    else: return False

def checkColor(elemColor, color):
    if elemColor == color: return True
    else: return False