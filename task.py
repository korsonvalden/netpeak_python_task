from selenium.webdriver.support.ui import WebDriverWait
import elements
import main
import os
import sys


try:
    myName = main.randomString()
    myLastName = main.randomString()
    myEmail = main.randomEmail()
    myPhoneNumber = str(main.randomInt())

    driver = main.getDriver()
    wait = WebDriverWait(driver, 10)
# 1. Перейти по ссылке на главную страницу сайта Netpeak. (https://netpeak.ua/)
    driver.get("https://netpeak.ua/")

# 2. Перейдите на страницу "Работа в Netpeak", нажав на кнопку "Карьера"
    elements.getCareerBtn().click()

# 3. Перейти на страницу заполнения анкеты, нажав кнопку - "Я хочу работать в Netpeak"
    elements.getFillFormBtn().click()

# 4. Загрузить файл с недопустимым форматом в блоке "Резюме", например png, и проверить что на странице появилось сообщение, о том что формат изображения неверный.
    elements.getDownloadResumeInput().send_keys(os.getcwd() + "\\netpeak_white_ru.svg")
    main.waitForText(elements.getResumeErrorLbl(), 10, "Ошибка: неверный формат файла")

# 5. Заполнить случайными данными блок "3. Личные данные"
    elements.getMyNameInput().send_keys(myName)
    if not main.checkEquality(myName, elements.getMyNameInput().get_attribute('value')):
        raise Exception("Expected Name: " + myName + " found: " + elements.getMyNameInput().get_attribute('value'))
    elements.getMyLastNameInput().send_keys(myLastName)
    if not main.checkEquality(myLastName, elements.getMyLastNameInput().get_attribute('value')):
        raise Exception("Expected Last Name: " + myLastName + " found: " + elements.getMyLastNameInput().get_attribute('value'))
    elements.getMyEmailInput().send_keys(myEmail)
    if not main.checkEquality(myEmail, elements.getMyEmailInput().get_attribute('value')):
        raise Exception("Expected Email: " + myEmail + " found: " + elements.getMyEmailInput().get_attribute('value'))
    elements.getMyPhoneNumInput().click()
    elements.getMyPhoneNumInput().send_keys(myPhoneNumber)
    if not main.checkEquality("38" + myPhoneNumber, elements.getMyPhoneNumInput().get_attribute('value')):
        raise Exception("Expected Phone Number: 38" + myPhoneNumber + " found: " + elements.getMyPhoneNumInput().get_attribute('value'))

    elements.getBirthDayPopup().click()
    day = str(main.randomDay())
    if len(day) == 1: day = "0" + day
    # elements.selectNumFromPopup(elements.getBirthDayPopup(), day)
    elements.getBirthDayPopup().find_element_by_xpath("//option[@value='" + day + "']").click()

    elements.getBirthMonthPopup().click()
    month = main.randomMonth()
    elements.getBirthMonthPopup().send_keys(month)

    elements.getBirthYearPopup().click()
    year = str(main.randomYear())
    elements.getBirthYearPopup().find_element_by_xpath("//option[@value='" + year + "']").click()

# 6. Нажать на кнопку отправить резюме
    elements.getSendBtn().click()

# 7. Проверить что сообщение на текущей странице  - "Все поля являются обязательными для заполнения" - подсветилось красным цветом
    elemColor = elements.getWarningFieldLbl().value_of_css_property("color")
    if not main.checkColor(elemColor, "rgba(255, 0, 0, 1)"):
        raise Exception("Expected Color: rgba(255, 0, 0, 1)" + " found: " + elemColor)

# 8. Нажать на логотип для перехода на главную страницу и убедиться что открылась нужная страница.
    elements.getNetpeakLogo().click()
    if not main.checkEquality(elements.getMainTitle().text, "Агентство интернет-маркетинга №1*"):
        raise Exception("Expected: Агентство интернет-маркетинга №1* found: " + elements.getMainTitle().text)
    # currentUrl = driver.current_url
    # if not main.checkEquality(currentUrl, "https://netpeak.ua/"):
    #     raise Exception("Expected: https://netpeak.ua/ found: " + currentUrl)

    main.closeDriver()
except:
    print("Unexpected error:", sys.exc_info()[0])
    main.closeDriver()
    raise